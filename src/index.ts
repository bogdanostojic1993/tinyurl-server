import express from "express";
import * as path from 'path';
import  bodyParser  from 'body-parser';
import mongoose  from 'mongoose';
import { nanoid } from 'nanoid';
import url from 'url';
const app = express();
const port = 8080;
app.use(bodyParser.json())
app.use( "/", express.static(path.join(__dirname, '../public')));

const baseURL = `http://localhost:8080`;

const LinkSchema = new mongoose.Schema({
    long_link: { type: String, required: true},
    short_link: { type: String},
    views: { type: Number, default: 0},
}, {timestamps: true})

const TinyUrl = mongoose.model('Links', LinkSchema)

app.get('/top-urls', async (req:express.Request, res:express.Response) => {

    const day = 24 * 60 * 60 * 1000;
    const start = new Date(new Date().getTime() - day);
    const top_links = await TinyUrl.find({"createdAt": {$gte: start }}).sort({views: -1})
    res.json(top_links)
})



app.post( "/api", async (req:express.Request, res:express.Response) => {
    let link = req.body.link;
    try {
        
        if(link.includes(`${baseURL}`)) {
            let url:any = await TinyUrl.findOne({short_link: link});
            console.log(url)
            if( url) {
                return res.send({
                    url: url.long_link
                })
            } else {
                return res.json({ code: 'Not valid URL'})
            }

        }
        let validLink = new URL(link);
        console.log(validLink)
        if(!validLink) 
            return res.status(400).json({ error: 'Not valid URL'})

        let url:any = await TinyUrl.findOne({long_link: link})
        if( url ) {
            return res.send({
                url: url.short_link
            })
        } else {
            url = await TinyUrl.create({
                long_link: req.body.link,
                short_link: `${baseURL}/${nanoid(6)}`
            })
            return res.json({
                url: url.short_link
            })
        }
    } catch (e) {

        res.json(e);
    }

});



app.get( ["/", "/admin", "/home"],  (req:express.Request, res:express.Response) => {
    console.dir(res.sendFile)
    res.sendFile(path.join(__dirname, '../public/index.html'));
});

app.get('/:short_url', async ( req:express.Request, res:express.Response) => {
    let shortURL = `${baseURL}/${req.params.short_url}`

    let url:any = await TinyUrl.findOneAndUpdate({short_link: shortURL}, {$inc: {'views': 1}})

    if( url ){
        res.redirect(url.long_link);
    } else {
        res.redirect(`/${shortURL}`);
    }
})



async function connectServer() {
    await mongoose.connect(`mongodb://localhost:27017/tinyurl`)
    app.listen(port, () => {
        console.log(`Express server listening on port ${port}`);
    });
}

connectServer();